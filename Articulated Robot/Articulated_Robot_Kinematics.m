function [J] = Articulated_Robot_Kinematics(Position)
    J =[0,0,0];
    if(Position(4)>0)
        clc;
        Len1 = 250;    
        Len2 = 390;    
        Len3 = 250;
        PosX = Position(1);
        PosY = Position(2);
        PosZ = Position(3);
        Link1 = Link([0,Len1,0,pi/2]); %(theta, d, a, alpha)-> a-x distance between coordinate joint, d-Z distance. 
                                        %alpha - twisting angle, theta - normal angle  
        Link2 = Link([0,0,Len2,0]);
        Link3 = Link([0,0,Len3,0]);
        Robot = SerialLink([Link1,Link2,Link3]);
        T = [ 1,0,0,PosX;
            0,1,0,PosY;
            0,0,1,PosZ;
            0,0,0,1 ];
        try
            J = Robot.ikine(T,"mask",[1 1 1 0 0 0]);
        catch
            J = [0,0,0];
        end
        J = [wrapToPi(J(1)),wrapToPi(J(2)),wrapToPi(J(3))];
        for i = 1:3
            if(abs(J(i))<0.1)
                J(i) = 0;
            end
        end
    end
end
