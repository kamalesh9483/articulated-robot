% Simscape(TM) Multibody(TM) version: 7.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(17).translation = [0.0 0.0 0.0];
smiData.RigidTransform(17).angle = 0.0;
smiData.RigidTransform(17).axis = [0.0 0.0 0.0];
smiData.RigidTransform(17).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0.054726052000069525 0.72011004640355236 0.19283403487695733];  % m
smiData.RigidTransform(1).angle = 0;  % rad
smiData.RigidTransform(1).axis = [0 0 0];
smiData.RigidTransform(1).ID = 'B[Link_2^Articulated_Robot_Assembly-1:-:End_Effector-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [0.019626971142855838 -0.026592024666887842 0.080000000000000016];  % m
smiData.RigidTransform(2).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(2).axis = [1 0 0];
smiData.RigidTransform(2).ID = 'F[Link_2^Articulated_Robot_Assembly-1:-:End_Effector-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [0.052463912730954201 0.21638525990589574 0.27283403487695734];  % m
smiData.RigidTransform(3).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(3).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(3).ID = 'B[Base^Articulated_Robot_Assembly-1:-:Link_1^Articulated_Robot_Assembly-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [0.052463912730954201 0.21638525990589574 0.27283403487695734];  % m
smiData.RigidTransform(4).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(4).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(4).ID = 'F[Base^Articulated_Robot_Assembly-1:-:Link_1^Articulated_Robot_Assembly-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [0.052463912730954188 0.33244077266053562 0.35283403487695736];  % m
smiData.RigidTransform(5).angle = 2.2204460492503131e-16;  % rad
smiData.RigidTransform(5).axis = [1 0 0];
smiData.RigidTransform(5).ID = 'B[Link_1^Articulated_Robot_Assembly-1:-:Link_2^Articulated_Robot_Assembly-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [0.052463912730954188 0.33244077266053562 0.35283403487695736];  % m
smiData.RigidTransform(6).angle = 2.2204460492503131e-16;  % rad
smiData.RigidTransform(6).axis = [1 0 0];
smiData.RigidTransform(6).ID = 'F[Link_1^Articulated_Robot_Assembly-1:-:Link_2^Articulated_Robot_Assembly-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-0.022536087269045754 0.12638525990589575 0.19783403487695733];  % m
smiData.RigidTransform(7).angle = 0;  % rad
smiData.RigidTransform(7).axis = [0 0 0];
smiData.RigidTransform(7).ID = 'AssemblyGround[Base^Articulated_Robot_Assembly-1:base_nut-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-0.022536087269045754 0.12638525990589575 0.34783403487695735];  % m
smiData.RigidTransform(8).angle = 0;  % rad
smiData.RigidTransform(8).axis = [0 0 0];
smiData.RigidTransform(8).ID = 'AssemblyGround[Base^Articulated_Robot_Assembly-1:base_nut-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [0.052463912730954201 0.086385259905895739 0.27283403487695734];  % m
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = 'AssemblyGround[Base^Articulated_Robot_Assembly-1:Base_0-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [0.12746391273095425 0.12638525990589575 0.19783403487695733];  % m
smiData.RigidTransform(10).angle = 0;  % rad
smiData.RigidTransform(10).axis = [0 0 0];
smiData.RigidTransform(10).ID = 'AssemblyGround[Base^Articulated_Robot_Assembly-1:base_nut-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [0.12746391273095414 0.12638525990589575 0.34783403487695735];  % m
smiData.RigidTransform(11).angle = 1.8692873511748433;  % rad
smiData.RigidTransform(11).axis = [-0 -1 -0];
smiData.RigidTransform(11).ID = 'AssemblyGround[Base^Articulated_Robot_Assembly-1:base_nut-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [0.052463912730954188 0.48788079358510661 0.19783403487695733];  % m
smiData.RigidTransform(12).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(12).axis = [0.70710678118654746 0.70710678118654757 0];
smiData.RigidTransform(12).ID = 'AssemblyGround[Link_2^Articulated_Robot_Assembly-1:Link_2_Right-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0.072758539863213165 0.5174005212959969 0.2728340348769574];  % m
smiData.RigidTransform(13).angle = 0;  % rad
smiData.RigidTransform(13).axis = [0 0 0];
smiData.RigidTransform(13).ID = 'AssemblyGround[Link_2^Articulated_Robot_Assembly-1:Motor-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [0.052463912730954118 0.48788079358510655 0.34783403487695735];  % m
smiData.RigidTransform(14).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(14).axis = [0 0 1];
smiData.RigidTransform(14).ID = 'AssemblyGround[Link_2^Articulated_Robot_Assembly-1:Link_2_Left-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [0.052463912730954194 0.21638525990589574 0.27283403487695734];  % m
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = 'AssemblyGround[Link_1^Articulated_Robot_Assembly-1:Link_1_Base-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [0.052463912730954188 0.33244077266053562 0.2157312311386396];  % m
smiData.RigidTransform(16).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(16).axis = [1 0 0];
smiData.RigidTransform(16).ID = 'AssemblyGround[Link_1^Articulated_Robot_Assembly-1:Link_1_1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0 0 0];  % m
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = 'SixDofRigidTransform[Base^Articulated_Robot_Assembly-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(8).mass = 0.0;
smiData.Solid(8).CoM = [0.0 0.0 0.0];
smiData.Solid(8).MoI = [0.0 0.0 0.0];
smiData.Solid(8).PoI = [0.0 0.0 0.0];
smiData.Solid(8).color = [0.0 0.0 0.0];
smiData.Solid(8).opacity = 0.0;
smiData.Solid(8).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.020433459341441529;  % kg
smiData.Solid(1).CoM = [0 -11.337269300701037 0];  % mm
smiData.Solid(1).MoI = [5.4487769854450949 2.6162141582103775 5.4487758322847357];  % kg*mm^2
smiData.Solid(1).PoI = [0 7.0979210911316141e-08 0];  % kg*mm^2
smiData.Solid(1).color = [0.090196078431372548 0.89803921568627454 1];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'base_nut*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 3.1350781918453756;  % kg
smiData.Solid(2).CoM = [0.0037556180204961049 39.163951472848879 -2.635764202847219];  % mm
smiData.Solid(2).MoI = [15863.983690819739 22665.754500281146 15496.496004350363];  % kg*mm^2
smiData.Solid(2).PoI = [337.05984453046409 0.60408800485041614 -0.22715269557425932];  % kg*mm^2
smiData.Solid(2).color = [0.90980392156862744 0.44313725490196076 0.031372549019607843];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'Base_0*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 2.9232297594189776;  % kg
smiData.Solid(3).CoM = [63.335496978598627 10.159310923692837 -1.7764547216760838e-05];  % mm
smiData.Solid(3).MoI = [7763.5246694297202 14948.687251296504 15027.810227437132];  % kg*mm^2
smiData.Solid(3).PoI = [-0.00032158540656411413 -0.0005605716902968589 -1942.7021099880362];  % kg*mm^2
smiData.Solid(3).color = [0.75294117647058822 0 0];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'End_Effector*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 1.6049255701174125;  % kg
smiData.Solid(4).CoM = [-20.532601366513397 0.78862035398988861 13.725195794523346];  % mm
smiData.Solid(4).MoI = [2601.0754259164792 41643.975383107369 41875.946431967619];  % kg*mm^2
smiData.Solid(4).PoI = [16.793649475522717 3142.5369183879448 -237.03868429783219];  % kg*mm^2
smiData.Solid(4).color = [0 0.75294117647058822 0];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'Link_2_Right*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.16867748030427016;  % kg
smiData.Solid(5).CoM = [-22.048760194486604 -9.1968911917098435 0];  % mm
smiData.Solid(5).MoI = [313.44053352501226 290.53864256173864 39.223628260248155];  % kg*mm^2
smiData.Solid(5).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(5).color = [0 0 0];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Motor*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 1.2120331279611052;  % kg
smiData.Solid(6).CoM = [27.491043537896516 1.0442686750303187 0.35327056778988347];  % mm
smiData.Solid(6).MoI = [1426.5488176511899 28813.664843383474 30060.235058103561];  % kg*mm^2
smiData.Solid(6).PoI = [-0.13013776577506575 -36.821662312689163 -176.25400209063199];  % kg*mm^2
smiData.Solid(6).color = [0 0.75294117647058822 0];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'Link_2_Left*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.82176134554515257;  % kg
smiData.Solid(7).CoM = [0 27.264655047745688 0];  % mm
smiData.Solid(7).MoI = [1223.7775488531752 2038.8954839904243 1358.7785860931174];  % kg*mm^2
smiData.Solid(7).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(7).color = [1 0 1];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'Link_1_Base*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 2.1614157456697782;  % kg
smiData.Solid(8).CoM = [0 57.102803738317739 0];  % mm
smiData.Solid(8).MoI = [6225.7988813740149 4918.4774584601819 6225.7988813740149];  % kg*mm^2
smiData.Solid(8).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(8).color = [1 0 1];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'Link_1_1*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(3).Rz.Pos = 0.0;
smiData.RevoluteJoint(3).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = 90.000000000000014;  % deg
smiData.RevoluteJoint(1).ID = '[Link_2^Articulated_Robot_Assembly-1:-:End_Effector-1]';

smiData.RevoluteJoint(2).Rz.Pos = 0;  % deg
smiData.RevoluteJoint(2).ID = '[Base^Articulated_Robot_Assembly-1:-:Link_1^Articulated_Robot_Assembly-1]';

smiData.RevoluteJoint(3).Rz.Pos = 0;  % deg
smiData.RevoluteJoint(3).ID = '[Link_1^Articulated_Robot_Assembly-1:-:Link_2^Articulated_Robot_Assembly-1]';

